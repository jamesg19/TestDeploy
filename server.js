const express = require('express');
const cors = require('cors');
const { createProxyMiddleware } = require('http-proxy-middleware');
const compression = require('compression'); // Importa el paquete de compresión

const app = express();


// Habilita la compresión antes de las rutas y las solicitudes de proxy
app.use(compression());

app.use(express.static('./dist/test-deploy/browser'));



app.get('/*', (req, res) =>
  res.sendFile('index.html', { root: 'dist/test-deploy/browser' })
);

app.listen(process.env.PORT || 4200, () => {
  console.log('Server is running on port 4200');
});
